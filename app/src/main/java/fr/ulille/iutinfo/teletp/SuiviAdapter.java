package fr.ulille.iutinfo.teletp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SuiviAdapter extends RecyclerView.Adapter<SuiviAdapter.ViewHolder> {
    final SuiviViewModel model;

    // TODO Q6.a
    class ViewHolder extends RecyclerView.ViewHolder {
        private final View itemView;

        public void setQuestion(String name) {
            ((TextView) itemView.findViewById(R.id.question)).setText(name);
        }

        public void setCheckbox(boolean b) {
            ((CheckBox) itemView.findViewById(R.id.checkBox)).setChecked(b);
        }

        public TextView getQuestion()  {
           return ((TextView) itemView.findViewById(R.id.question));
        }
        public TextView getCheckbox()  {
            return ((TextView) itemView.findViewById(R.id.checkBox));
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            this.itemView.setOnClickListener(view -> {
                select(getAdapterPosition());
            });
        }
    }
    // TODO Q7
    public SuiviAdapter(SuiviViewModel model) {
        this.model = model;
    }

    @NonNull
    @Override
    public SuiviAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.question_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setQuestion(model.getQuestions(position));
    }

    public void select(int position) {
        Integer old = model.getNextQuestion();
        if (old != null) {
            notifyItemChanged(old);
        }
        model.setNextQuestion(position);

        notifyItemChanged(position);
    }



    @Override
    public int getItemCount() {
        return model.getCount();
    }
}
