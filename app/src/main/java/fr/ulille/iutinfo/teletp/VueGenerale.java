package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.util.Log;
import android.view.DragAndDropPermissions;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

public class VueGenerale extends Fragment {


    // TODO Q1
    private String salle;
    private String poste;
    private String DISTANCIEL;
    // TODO Q2.c
    private SuiviViewModel model;
    private Spinner spinnerSalle;
    private Spinner spinnerPoste;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        DISTANCIEL = getActivity().getResources().getStringArray(R.array.list_salles)[0];
        // TODO Q2.c
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);

        // TODO Q4

        spinnerSalle = (Spinner) view.findViewById(R.id.spSalle);
        spinnerPoste = (Spinner) view.findViewById(R.id.spPoste);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.list_salles, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSalle.setAdapter(adapter);

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getContext(),
                R.array.list_postes, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPoste.setAdapter(adapter2);


        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            String text = ((EditText) view.findViewById(R.id.tvLogin)).getText().toString();
            model.setUsername(
                    text
            );

            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        spinnerSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // TODO Q5.b
        update();
        // TODO Q9
    }

    // TODO Q5.a
    private void update() {
        String text = spinnerSalle.getSelectedItem().toString();

        if (text.equals(getActivity().getResources().getStringArray(R.array.list_salles)[0])) {
            spinnerPoste.setVisibility(View.INVISIBLE);
            spinnerPoste.setEnabled(false);
            model.setLocalisation(spinnerSalle.getSelectedItem().toString());

        } else {
            spinnerPoste.setVisibility(View.VISIBLE);
            spinnerPoste.setEnabled(true);
            model.setLocalisation(spinnerSalle.getSelectedItem().toString() + " : " + spinnerPoste.getSelectedItem().toString());
        }
    }

    // TODO Q9
}